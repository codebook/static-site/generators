# Static Site Generators


Static website - blog site generators.



## Generator tools

### Javascript

* [Metalsmith](http://www.metalsmith.io/)
* [Assemble](http://assemble.io/)
* [Hexo](https://hexo.io/)
* [harp](http://harpjs.com/)
* [NUXT](https://nuxtjs.org/)
* [Gatsby](https://www.gatsbyjs.org/)



### GoLang

* [Hugo](https://gohugo.io/)


### Python

* [Pelican](https://blog.getpelican.com/)
* [Sphinx](http://www.sphinx-doc.org/en/stable/)
* [Nikola](https://getnikola.com/)
* [Cactus](https://github.com/eudicots/Cactus)
* [Lektor](https://www.getlektor.com/)



### Ruby

* [Jekyll](http://jekyllrb.com/)
* [Middleman](https://middlemanapp.com/)
* [nanoc](https://nanoc.ws/)
* [Octopress](http://octopress.org/)



